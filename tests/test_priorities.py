###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from unittest.mock import patch

from lb.nightly.rpc import get_task_priority

slots = [
    "celery-test",
    "lhcb-2016-patches",
    "lhcb-2017-patches",
    "lhcb-2018-patches",
    "lhcb-coverity",
    "lhcb-dd4hep",
    "lhcb-digi14-patches",
    "lhcb-g4-dev",
    "lhcb-gaudi-head",
    "lhcb-gauss-conf",
    "lhcb-gauss-dev",
    "lhcb-gauss-gen-dev",
    "lhcb-gauss-gen2-dev",
    "lhcb-gauss-gen3-dev",
    "lhcb-gauss-lamarr",
    "lhcb-gaussino",
    "lhcb-gaussino-fastsim",
    "lhcb-gaussino-new-cmake",
    "lhcb-gaussino-prerelease",
    "lhcb-head",
    "lhcb-head-2",
    "lhcb-hlt2011-patches",
    "lhcb-hlt2012-patches",
    "lhcb-hlt2016-patches",
    "lhcb-lcg-dev3",
    "lhcb-lcg-dev4",
    "lhcb-lcg-rc",
    "lhcb-lcg-test",
    "lhcb-master",
    "lhcb-prerelease",
    "lhcb-reco14-patches",
    "lhcb-reco15-patches",
    "lhcb-run2-gaudi-head",
    "lhcb-run2-patches",
    "lhcb-run2-patches-dev4",
    "lhcb-run2-prerelease",
    "lhcb-run3-cleanup",
    "lhcb-sanitizers",
    "lhcb-sim09",
    "lhcb-sim09-cmake",
    "lhcb-sim10",
    "lhcb-stripping21-firstpass-patches",
    "lhcb-stripping21-patches",
    "lhcb-stripping24-patches",
    "lhcb-test-davinci-51",
    "lhcb-test-davinci-v42r7p3",
    "quarantine-Lovell",
    "quarantine-bender",
    "quarantine-orwell",
]

projects = [
    "Gaudi",
    "Detector",
    "LHCb",
    "Online",
    "Lbcom",
    "Boole",
    "Rec",
    "Brunel",
    "Allen",
    "Moore",
    "Analysis",
    "DaVinci",
    "Panoramix",
    "Panoptes",
    "Alignment",
    "Noether",
    "Kepler",
    "AlignmentOnline",
    "LHCbIntegrationTests",
    "MooreAnalysis",
    "MooreOnline",
    "Run2Support",
    "Geant4",
    "Gauss",
]

platforms = [
    "x86_64_v2-centos7-gcc11-opt",
    "x86_64_v2-centos7-gcc11-dbg",
    "x86_64_v2-centos7-clang12-opt",
    "x86_64_v2-centos7-clang12-dbg",
    "x86_64_v2-centos7-gcc10-opt",
    "x86_64_v2-centos7-gcc10-dbg",
    "x86_64_v3-centos7-gcc11-opt+g",
    "x86_64_v4+vecwid256-centos7-gcc11-opt",
]


@patch("requests.get")
def test_priorities(mock_get):
    mock_get.return_value.text = """
default:
    slot: 0
    project: 0.8
    platforms: 0.8

slots:
    lhcb-2018-patches: 0.8
    lhcb-gaudi-head: 0.9
    lhcb-master: 1.0
    lhcb-head: 0.8
    lhcb-sim10: 0.8
    lhcb-gauss-gen-dev: 0.5

projects:
    Moore: 1.0
    DaVinci: 0.8
    Gauss: 0.5

platforms:
    x86_64_v2-centos7-gcc11-opt: 1.0
    x86_64-slc6-gcc8-opt: 0.1

overrides:
    lhcb-head:
        DaVinci:
            x86_64_v2-centos7-gcc11-opt: 0.95
    """
    priorities = {}
    for slot in slots:
        for project in projects:
            for platform in platforms:
                priorities[(slot, project, platform)] = get_task_priority(
                    slot, project, platform
                )
    assert priorities[("lhcb-master", "Moore", "x86_64_v2-centos7-gcc11-opt")] == 100
    assert priorities[("lhcb-head", "DaVinci", "x86_64_v2-centos7-gcc11-opt")] == 95
    assert all(0 < priority <= 100 for priority in priorities.values())

    assert get_task_priority("lhcb-head") == 51
    assert get_task_priority("lhcb-gaudi-head") == 58

    assert get_task_priority("lhcb-head", "Moore") == 64
    assert get_task_priority("lhcb-gaudi-head", "Gauss") == 36

    assert get_task_priority("my-slot") == 51
    assert get_task_priority("my-slot", "my-project") == 51
    assert get_task_priority("my-slot", "my-project", "my-platform") == 51
