###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import contextlib
import os
from contextlib import contextmanager
from subprocess import CalledProcessError
from tempfile import TemporaryDirectory
from unittest.mock import ANY, PropertyMock, call, patch

import pytest
from lb.nightly.configuration import Project, Slot

from lb.nightly.rpc.tasks import checkout

patch_run_ok = patch(
    "lb.nightly.rpc.tasks.run",
    **{"return_value.returncode": 0},
)


class MockConda:
    def __init__(
        self,
        base_dir=TemporaryDirectory().name,
        environment={},
    ):
        self.base_dir = base_dir
        self.environment = environment
        self.python = "python"
        self.bin_dir = base_dir
        self.prefix = base_dir

    @contextmanager
    def __call__(self):
        yield self

    def dump_environment(self):
        pass


patch_work_env = patch("lb.nightly.rpc.tasks.CondaEnv", side_effect=MockConda)


@contextlib.contextmanager
def working_directory(path):
    """A context manager which changes the working directory to the given
    path, and then changes it back to its previous value on exit.

    """
    prev_cwd = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(prev_cwd)


@patch_work_env
@patch(
    "lb.nightly.rpc.tasks.get",
    return_value=Slot(
        "test-slot", build_id=123, projects=[Project("Moore", "HEAD")]
    ).Moore,
)
@patch_run_ok
@patch("lb.nightly.rpc.tasks.current_task")
def test_success(task, run, get, work_env, tmp_path):
    type(task.request).id = PropertyMock(return_value="abc123")
    with working_directory(tmp_path):
        os.environ["CELERY_LOG_FILE"] = str(tmp_path)
        checkout("nightly/lhcb-head/999/Gaudi")
        run.assert_has_calls(
            [
                call(
                    [
                        ANY,
                        "-m",
                        "lb.nightly.functions.rpc",
                        "checkout",
                        "nightly/lhcb-head/999/Gaudi",
                        "abc123",
                    ],
                    cwd=ANY,
                    capture_output=True,
                    check=True,
                    env=ANY,
                )
            ]
        )
        logs = os.path.join(
            os.path.dirname(os.environ["CELERY_LOG_FILE"]), "checkout_abc123.log"
        )
        assert os.path.exists(logs)
        with open(logs) as lf:
            assert "exited with returncode: 0" in lf.read()


@patch_work_env
@patch(
    "lb.nightly.rpc.tasks.get",
    return_value=Slot(
        "test-slot", build_id=123, projects=[Project("Moore", "HEAD")]
    ).Moore,
)
@patch(
    "lb.nightly.rpc.tasks.run",
)
@patch("lb.nightly.rpc.tasks.current_task")
def test_fail(task, run, get, work_env, tmp_path):
    cpe = CalledProcessError(
        cmd=[],
        returncode=22,
    )
    cpe.args = (22, [])
    cpe.stdout = b""
    cpe.stderr = b""
    run.side_effect = cpe
    type(task.request).id = PropertyMock(return_value="abc123")
    with working_directory(tmp_path):
        os.environ["CELERY_LOG_FILE"] = str(tmp_path)
        with pytest.raises(CalledProcessError):
            checkout("nightly/lhcb-head/999/Gaudi")
        run.assert_has_calls(
            [
                call(
                    [
                        ANY,
                        "-m",
                        "lb.nightly.functions.rpc",
                        "checkout",
                        "nightly/lhcb-head/999/Gaudi",
                        "abc123",
                    ],
                    cwd=ANY,
                    capture_output=True,
                    check=True,
                    env=ANY,
                )
            ]
        )
        logs = os.path.join(
            os.path.dirname(os.environ["CELERY_LOG_FILE"]), "checkout_abc123.log"
        )
        assert os.path.exists(logs)
        with open(logs) as lf:
            assert "exited with returncode: 22" in lf.read()
