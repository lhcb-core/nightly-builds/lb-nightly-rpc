###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from socket import gethostname

from lb.nightly.rpc import _build, _checkout, _start_slot, _test, app
from lb.nightly.rpc.archs import archs

active_queues = app.control.inspect().active_queues()
assert active_queues


def test_workers_queues():
    for worker, queues in active_queues.items():
        for queue in queues:
            assert queue["name"].split("-")[0] in worker


def test_checkout_routing():
    _checkout.apply_async(args=["project"], countdown=1)
    scheduled = app.control.inspect().scheduled()
    assert scheduled
    assert f"checkout@{gethostname()}" in scheduled.keys()
    assert scheduled[f"checkout@{gethostname()}"]
    for task in scheduled[f"checkout@{gethostname()}"]:
        assert "checkout" in task["request"]["name"]


def test_build_routing():
    _build.apply_async(args=["project", f"{archs()[0]}-os-comp"], countdown=1)
    scheduled = app.control.inspect().scheduled()
    assert scheduled
    assert f"build@{gethostname()}" in scheduled.keys()
    assert scheduled[f"build@{gethostname()}"]
    for task in scheduled[f"build@{gethostname()}"]:
        assert "build" in task["request"]["name"]


def test_test_routing():
    _test.apply_async(args=["project", f"{archs()[0]}-os-comp"], countdown=1)
    scheduled = app.control.inspect().scheduled()
    assert scheduled
    assert f"test@{gethostname()}" in scheduled.keys()
    assert scheduled[f"test@{gethostname()}"]
    for task in scheduled[f"test@{gethostname()}"]:
        assert "test" in task["request"]["name"]


def test_start_slot_routing():
    task = _start_slot.apply_async(args=["slot"], countdown=10)
    scheduled = app.control.inspect().scheduled()
    assert scheduled
    assert f"scheduler@{gethostname()}" in scheduled.keys()
    assert scheduled[f"scheduler@{gethostname()}"]
    for task in scheduled[f"scheduler@{gethostname()}"]:
        assert "scheduler" in task["request"]["name"]


def test_no_route():
    res = _test.apply_async(args=["project", "arch-os-comp"], countdown=1)
    scheduled = app.control.inspect().scheduled()
    for task in scheduled[f"build@{gethostname()}"]:
        assert not task["request"]["id"] == res.id
